﻿using FPS.BusinessLayer.ViewModel.Company;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.Repository.Company
{
    public interface ICompanyRepository
    {
        public void AddCompany(CompanyModel model);
        public void EditCompany(CompanyModel model);
        public void DeleteCompany(Guid id);
        public List<FPS.DataLayer.Entity.Company> getList();
        public FPS.DataLayer.Entity.Company getById(Guid id);

    }
}
