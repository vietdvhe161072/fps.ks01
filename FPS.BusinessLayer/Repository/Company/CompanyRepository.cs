﻿using FPS.BusinessLayer.ViewModel.Company;
using FPS.DataLayer.Context;
using FPS.DataLayer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.Repository.Company
{
    public class CompanyRepository : BaseRepository, ICompanyRepository
    {
        public CompanyRepository(FPSContext context) : base(context)
        {
        }

        public void AddCompany(CompanyModel model)
        {
            FPS.DataLayer.Entity.Company company = new FPS.DataLayer.Entity.Company();
            company.Id = Guid.NewGuid();
            company.CompanyName = model.CompanyName;
            company.CreateDate = DateTime.Now;
            company.UpdateDate = DateTime.Now;
            company.CreateBy = model.CreateBy;
            company.UpdateBy = model.UpdateBy;
            company.IsDeleted = false;
            _context.Companies.Add(company);
            _context.SaveChanges();
        }

        public void EditCompany(CompanyModel model)
        {
            FPS.DataLayer.Entity.Company company = _context.Companies.FirstOrDefault(x => x.Id == model.Id);
            company.CompanyName = model.CompanyName;
            company.CreateDate = model.CreateDate;
            company.UpdateDate = model.UpdateDate;
            company.CreateBy = model.CreateBy;
            company.UpdateBy = model.UpdateBy;
            company.IsDeleted = false;
            _context.Companies.Update(company);
            _context.SaveChanges();
        }
        public void DeleteCompany(Guid id)
        {
            FPS.DataLayer.Entity.Company company = _context.Companies.FirstOrDefault(x => x.Id == id);
            if (company != null)
            {
                company.IsDeleted = true;
                _context.SaveChanges();
            }
        }
    //    public void deletecompany(guid id)
    //    {
    //        fps.datalayer.entity.company company = _context.companies.firstordefault(x => x.id == id);
    //        _context.companies.remove(company);
    //        _context.savechanges();
    //    }
    //    public list<fps.datalayer.entity.company> getlist()
    //    {
    //        list<fps.datalayer.entity.company> x = _context.companies.tolist();
    //        return x;
    //    }
    //    public fps.datalayer.entity.company getbyid(guid id)
    //    {
    //        return _context.companies.firstordefault(x => x.id == id);
    //    }
    //}
    public List<FPS.DataLayer.Entity.Company> getList()
        {
            List<FPS.DataLayer.Entity.Company> companies = _context.Companies.Where(x => !x.IsDeleted).ToList();
            return companies;
        }

        public FPS.DataLayer.Entity.Company getById(Guid id)
        {
            return _context.Companies.FirstOrDefault(x => x.Id == id && !x.IsDeleted);
        }
    }
}
