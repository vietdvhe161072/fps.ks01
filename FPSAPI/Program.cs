using FPS.DataLayer.Context;
using Microsoft.EntityFrameworkCore;
using FPS.BusinessLayer.Repository.Company;
using Microsoft.AspNetCore.Identity;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddDbContext<FPSContext>(x => x.UseSqlServer(builder.Configuration.GetConnectionString("FPSConnectionString")));
// For Identity
builder.Services.AddIdentity<IdentityUser, IdentityRole>()
    .AddEntityFrameworkStores<FPSContext>()
    .AddDefaultTokenProviders();
builder.Services.AddScoped<ICompanyRepository, CompanyRepository>();
var app = builder.Build();



// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
