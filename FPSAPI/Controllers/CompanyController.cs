﻿using FPS.BusinessLayer.Repository.Company;
using FPS.BusinessLayer.ViewModel.Company;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FPS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyController : ControllerBase
    {
        public readonly ICompanyRepository _companyRepository;
        public CompanyController(ICompanyRepository companyRepository)
        {
            _companyRepository = companyRepository;
        }

        [HttpPost]
        [Route("AddCompany")]
        public void AddCompany(CompanyModel model)
        {
            _companyRepository.AddCompany(model);
        }
        [HttpPut]
        [Route("EditCompany")]
        public void EditCompany(CompanyModel model)
        {
            _companyRepository.EditCompany(model);
        }

        [HttpDelete]
        [Route("DeleteCompany")]
        public void DeleteCompany(Guid model)
        {
            _companyRepository.DeleteCompany(model);
        }

        [HttpGet]
        [Route("List")]
        public List<FPS.DataLayer.Entity.Company> List()
        {
           return _companyRepository.getList();
        }

        [HttpGet]
        [Route("Detail")]
        public FPS.DataLayer.Entity.Company Detail(Guid model)
        {
            return _companyRepository.getById(model);
        }
    }
}
